#! /bin/sh
echo "Building Projects"
echo
./mvnw clean install

echo "Running Embedded Engine"
echo
./mvnw exec:java -pl :embedded-engine
