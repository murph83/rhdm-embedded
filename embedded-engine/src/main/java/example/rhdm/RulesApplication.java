package example.rhdm;

import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RulesApplication {

    private static final Logger LOG = LoggerFactory.getLogger(RulesApplication.class);
    private final KieContainer kieContainer;

    RulesApplication(KieContainer kieContainer){
        this.kieContainer = kieContainer;
    }

    private void run(){
        KieSession session = kieContainer.newKieSession();

        session.insert(new Input("Hello Rules!"));

        session.fireAllRules();

        session.dispose();
    }

    public static void main(String...args){

        LOG.info("Running RulesApplication");

        KieServices kieServices = KieServices.get();

        ReleaseId releaseId = kieServices.newReleaseId("example.rhdm", "kjar", "7.1.1");
        KieContainer kieContainer = kieServices.newKieContainer(releaseId);

        new RulesApplication(kieContainer).run();

        LOG.info("RulesApplication Complete");
    }



}
