Red Hat Decision Manager Embedded Example
=========================================

This project demonstrates the use of Maven to build and run a Red Hat Decision Manager (Drools) project.

The project contains two modules
`kjar` Contains the DRL file, Domain model, and kmodule.xml. The Maven pom is configured to build the kjar

This project uses [Maven Wrapper](https://github.com/takari/maven-wrapper) to embed maven in the repo, which means you don't need Maven installed to run it.

Running `demo.sh` or `demo.bat` results in the kjar and embedded-engine projects being built, then the embedded-engine being executed. For the default configuration, expect to see output like the following:
[![demo script execution](https://asciinema.org/a/a2n8mx4SZjHyrF0dGHD2xNyIa.svg)](https://asciinema.org/a/a2n8mx4SZjHyrF0dGHD2xNyIa)
